﻿using BlogAPI.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                using (BlogDBContext db = new BlogDBContext())
                {
                    var lst = (from item in db.Posts
                               orderby item.Fecha descending
                               select new { item.Id, item.Titulo, item.Imagen, item.Categoria, item.Fecha }).ToList();
                    return Ok(lst);
                }
            }
            catch
            {
                return StatusCode(500, "Error interno del sistema");
            }
        }

        [HttpGet("{Id:int}")]
        public ActionResult Get(int id)
        {
            try
            {
                using (BlogDBContext db = new BlogDBContext())
                {
                    Posts post = db.Posts.Find(id);
                    if (post == null)
                    {
                        return StatusCode(404, "El elemento no existe");
                    }
                    return Ok(post);
                }
            }
            catch
            {
                return StatusCode(500, "Error interno del sistema");
            }
        }

        [HttpPost]
        public ActionResult Post([FromBody] Posts model)
        {
            try
            {
                using (BlogDBContext db = new BlogDBContext())
                {
                    Posts post = new Posts();
                    post.Titulo = model.Titulo;
                    post.Contenido = model.Contenido;
                    post.Imagen = model.Imagen;
                    post.Categoria = model.Categoria;
                    db.Posts.Add(post);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return StatusCode(500, "Error interno del sistema");
            }
        }

        [HttpPatch("{Id:int}")]
        public IActionResult Patch(int id, [FromBody] JsonPatchDocument<Posts> patchEntity)
        {
            try
            {
                using (BlogDBContext db = new BlogDBContext())
                {
                    var post = db.Posts.Find(id);
                    if (post == null)
                    {
                        return StatusCode(404, "El elemento no existe");
                    }
                    patchEntity.ApplyTo(post, ModelState);
                    db.Entry(post).State = EntityState.Modified;
                    db.SaveChanges();
                    return Ok(post);
                }
            }
            catch
            {
                return StatusCode(500, "Error interno del sistema");
            }
        }

        [HttpDelete("{Id:int}")]
        public ActionResult Delete(int id)
        {
            try
            {
                using (BlogDBContext db = new BlogDBContext())
                {
                    Posts post = db.Posts.Find(id);
                    if (post == null)
                    {
                        return StatusCode(404, "El elemento no existe");
                    }
                    db.Posts.Remove(post);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return StatusCode(500, "Error interno del sistema");
            }
        }
    }
}
