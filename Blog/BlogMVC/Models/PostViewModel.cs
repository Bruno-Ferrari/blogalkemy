﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogMvc.Models
{
    public class PostViewModel
    {
        public int Id { get; set; } 

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Contenido { get; set; }

        public byte[] Imagen { get; set; }
        public int Categoria { get; set; }
        public DateTime Fecha { get; set; }
    }
}
