﻿using BlogMvc.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BlogMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly string apiBaseUrl;
        private readonly IConfiguration _Configure;

        public HomeController(IConfiguration configuration)
        {
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("ApiUrl");
        }

        public async Task<IActionResult> Index()
        {
            List<PostViewModel> posts = new List<PostViewModel>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync(apiBaseUrl))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    posts = JsonConvert.DeserializeObject<List<PostViewModel>>(apiResponse);
                }
            }
            return View(posts);
        }

        public async Task<IActionResult> Detalle(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            PostViewModel post = new PostViewModel();
            try
            {
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync(apiBaseUrl + id))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        post = JsonConvert.DeserializeObject<PostViewModel>(apiResponse);
                    }
                }
            }
            catch
            {
                return NotFound();
            }
            return View(post);
        }

        public ActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Crear(PostViewModel new_post)
        {
            if (ModelState.IsValid)
            {
                PostViewModel post = new PostViewModel();
                try
                {
                    using (var httpClient = new HttpClient())
                    {
                        StringContent content = new StringContent(JsonConvert.SerializeObject(new_post), Encoding.UTF8, "application/json");
                        using (var response = await httpClient.PostAsync(apiBaseUrl, content))
                        {
                            string apiResponse = await response.Content.ReadAsStringAsync();
                            post = JsonConvert.DeserializeObject<PostViewModel>(apiResponse);
                        }
                    }
                }
                catch
                {
                    return BadRequest();
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        public ActionResult Editar()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editar(int? id, PostViewModel patch_post)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    string json = $"[{{ \"value\":\"" + patch_post.Titulo + "\", \"path\":\"/titulo\", \"op\":\"replace\"}," +
                                    " { \"value\":\"" + patch_post.Contenido + "\", \"path\":\"/contenido\", \"op\":\"replace\"}]";
                    var request = new HttpRequestMessage
                    {
                        RequestUri = new Uri(apiBaseUrl + id),
                        Method = new HttpMethod("patch"),
                        Content = new StringContent(json, Encoding.UTF8, "application/json-patch+json")
                    };
                    var response = await httpClient.SendAsync(request);
                }
            }
            catch
            {
                return StatusCode(500);

            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Eliminar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Eliminar(int? id, IFormCollection collection)
        {
            if (id == null)
            {
                return NotFound();
            }
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.DeleteAsync(apiBaseUrl + id);
                    if (!response.IsSuccessStatusCode)
                    {
                        return NotFound();
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return StatusCode(500);
            }
        }

    }
}
